pub fn my_last(list:&[i32])->i32{
    return match list.last() {
        Some(&number)=>number,
        None => -1,
    };
}
