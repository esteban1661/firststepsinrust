mod nn_exercises;

fn main() {
    println!("The main goal of this project is put the hands on Rust language. 
    Use test functions to play with the functionalities programmed in 99Exercises.rs file.");
}

#[test]
fn my_last_test() {
    assert_eq!(nn_exercises::my_last(&[1,2,3,4,5]),5);
    assert_eq!(nn_exercises::my_last(&[1]),1);
    assert_eq!(nn_exercises::my_last(&[10,11,13]),13);
    assert_eq!(nn_exercises::my_last(&[1,12,-10]),-10);
}